<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->
# An Easy To Use And Maintain Calendar

[![pipeline status](https://gitlab.com/jb-packages/flutter/easy_calendar/badges/main/pipeline.svg)](https://gitlab.com/jb-packages/flutter/easy_calendar/-/pipelines)
[![pub package](https://img.shields.io/pub/v/easy_calendar.svg)](https://pub.dev/packages/easy_calendar)

`easy_calendar` - because I wanted to create an easy to use and _easy to maintain_ calendar package.

This calendar package is using UTC everywhere. So it is your responsibility to get the timezone stuff right.

Using UTC has the big advantage of not having problems with day light saving time etc., which in turn makes it way easier to develop and maintain the package.
Also it makes it easier for other users to adapt and extend the package and therefore helps to contribute to the package.
And since I am using the [`rrule` package](https://pub.dev/packages/rrule), I don't have to worry about recurrence calculations being wrong.

## Features

v0.1.0 is a placeholder for the package.

## Getting started

See [Installing](https://pub.dev/packages/easy_calendar/install) for basic installation instructions.

## Usage

Examples for usage will follow, once I have the first prototype running.

My idea right now is:

We follow a MVC pattern, seperating

### ⚠️ Consider this! ⚠️
* If you have problems with the events not updating the way you want them to,
  make sure that a call to `listEquals` returns `false` when you compare the
  previous to the updated list. This includes having a correctly working
  [== Operator](https://api.flutter.dev/flutter/dart-core/Object/operator_equals.html)
  (It bit me, so I'm writing it down for you)

## Additional information

### Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

### Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

### Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

### Authors and acknowledgment

Main author is @jonasbadstuebner.  
There are no other contributors yet.

### License

For the projects license see [LICENSE.md](https://gitlab.com/jb-packages/flutter/easy_calendar/-/blob/main/LICENSE.md)

### Project status

The project is an early WIP.
