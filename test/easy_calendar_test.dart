// ignore: lines_longer_than_80_chars
// ignore_for_file: prefer-moving-to-variable, avoid-long-functions, prefer-match-file-name, no-magic-number, avoid-ignoring-return-values, avoid_print, avoid-duplicate-test-assertions

import 'dart:async';

import 'package:easy_calendar/easy_calendar.dart';
import 'package:easy_calendar/src/extensions/utc_date_time_extension.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:utc_date_time/utc_date_time.dart';

/// General hint(s):
///
///  - The 01.01.2000 (which I use a lot in tests) has been a Saturday.
///

void main() {
  group('UtcDateTime tests', () {
    const utcDateTime = UtcDateTime(2000);
    final earlier = utcDateTime.diffDays(-2);
    final same = utcDateTime.copyWith();
    final laterThatDay = utcDateTime.add(const Duration(hours: 1));
    final oneWeekLater = utcDateTime.add(const Duration(days: 7));
    test('does parse correctly', () {
      expect(utcDateTime.toString(), equals('2000-01-01 00:00:00.000000Z'));
      expect(
        utcDateTime.microsecondsSinceEpoch,
        equals(DateTime.utc(2000).microsecondsSinceEpoch),
      );
    });

    test('isBefore and isAfter working as expected', () {
      /// check earlier
      expect(utcDateTime.isAfter(earlier), equals(true));
      expect(
        utcDateTime.isSameOrAfter(earlier, precision: Precision.day),
        equals(true),
      );
      expect(utcDateTime.isBefore(earlier), equals(false));

      /// check same
      expect(utcDateTime.isAfter(same), equals(false));
      expect(
        utcDateTime.isSameOrAfter(same, precision: Precision.day),
        equals(true),
      );
      expect(utcDateTime.isBefore(same), equals(false));

      /// check laterThatDay
      expect(utcDateTime.isAfter(laterThatDay), equals(false));
      expect(
        utcDateTime.isSameOrAfter(laterThatDay, precision: Precision.day),
        equals(true),
      );
      expect(utcDateTime.isBefore(laterThatDay), equals(true));

      /// check oneWeekLater
      expect(utcDateTime.isAfter(oneWeekLater), equals(false));
      expect(
        utcDateTime.isSameOrAfter(oneWeekLater, precision: Precision.day),
        equals(false),
      );
      expect(utcDateTime.isBefore(oneWeekLater), equals(true));
    });

    test('date calculations are working', () {
      expect(
        utcDateTime.prevMonthStart,
        equals(const UtcDateTime(1999, 12)),
      );

      expect(
        utcDateTime.dayStart.difference(utcDateTime.dayEnd).inSeconds,
        equals(Duration.secondsPerDay - 1),
      );

      expect(utcDateTime.nextMonthStart, equals(const UtcDateTime(2000, 2)));
      expect(
        utcDateTime.nextMonthStart.difference(utcDateTime.monthEnd).inSeconds,
        equals(-1),
      );
    });

    test('range functions are working', () {
      /// check clamp
      expect(
        utcDateTime.clamp(laterThatDay, oneWeekLater),
        equals(laterThatDay),
      );
      expect(
        oneWeekLater.clamp(laterThatDay, utcDateTime),
        equals(utcDateTime),
      );

      expect(
        utcDateTime.isSameOrAfter(earlier, precision: Precision.day),
        equals(true),
      );
      expect(utcDateTime.isBefore(earlier), equals(false));
    });
  });

  group('EasyCalendarController tests', () {
    test('loads appointments correctly', () async {
      final testCalendarController = _TestCalendarController(
        initialMinimumDate: const UtcDateTime(2000),
        initialMaximumDate: const UtcDateTime(2000, 3),
        datasource: _TestDataSource(),
      );
      expect(
        testCalendarController.appointments
            .where(
              (final appointment) =>
                  appointment.uuid == _TestAppointmentTypes.singleOne,
            )
            .length,
        equals(1),
      );

      expect(
        testCalendarController.appointments
            .where(
              (final appointment) =>
                  appointment.uuid == _TestAppointmentTypes.sometimesThere,
            )
            .length,
        equals(0),
      );

      await testCalendarController.refresh();

      await testCalendarController.refresh();

      await testCalendarController.refresh();

      expect(
        testCalendarController.appointments
            .where(
              (final appointment) =>
                  appointment.uuid == _TestAppointmentTypes.sometimesThere,
            )
            .length,
        equals(1),
      );
    });

    test('notifies correctly', () async {
      final testCalendarController = _TestCalendarController(
        initialMinimumDate: const UtcDateTime(2000),
        initialMaximumDate: const UtcDateTime(2000, 3),
        datasource: _TestDataSource(),
      );

      int loadingStateChanges = 0;
      int wasInLoadingState = 0;
      int specificDateReloadCounter = 0;

      testCalendarController.isRefreshing.addListener(() {
        loadingStateChanges += 1;
        if (testCalendarController.isRefreshing.value) {
          wasInLoadingState += 1;
        }
      });

      testCalendarController.addListenerForDate(
        () {
          final appointmentUuids = testCalendarController
              .getAppointmentsForDate(const UtcDateTime(2000));
          print('appointmentUuids: $appointmentUuids');
          specificDateReloadCounter += 1;
        },
        const UtcDateTime(2000),
      );

      await testCalendarController.refresh();

      /// Goes into loading state;
      /// goes into not-loading state;
      /// Total state changes => +2
      expect(loadingStateChanges, equals(2));
      expect(wasInLoadingState, equals(1));
      expect(specificDateReloadCounter, equals(1));

      await testCalendarController.refresh();

      expect(loadingStateChanges, equals(4));
      expect(wasInLoadingState, equals(2));

      /// With my test setup, the appointments for 01.01.2000
      /// don't change, they stay at the initial value always.
      /// So this should not reload.
      expect(specificDateReloadCounter, equals(1));
    });
  });
}

class _TestAppointment extends EasyAppointment {
  @override
  final UtcDateTime startTime;

  @override
  final UtcDateTime endTime;

  @override
  final String uuid;

  @override
  final String? recurrenceRule;

  @override
  String get subject => 'uuid-$uuid';

  const _TestAppointment({
    required this.uuid,
    required this.startTime,
    required this.endTime,
    // I made the recurrenceRule to be required, so the tests
    // are more explicit.
    required this.recurrenceRule,
  });
}

class _TestAppointmentTypes {
  static const singleOne = 'singleOne';
  static const simpleRecurrence = 'simpleRecurrence';
  static const changingCount = 'changingCount';
  static const sometimesThere = 'sometimesThere';
}

class _TestDataSource extends EasyAppointmentDataSource<_TestAppointment> {
  int reloadCounter = 0;

  @override
  FutureOr<List<_TestAppointment>> loadAppointments() {
    reloadCounter += 1;

    final testAppointments = <_TestAppointment>[
      /// Two appointments stay the same all the time.
      _TestAppointment(
        uuid: _TestAppointmentTypes.singleOne,
        startTime: const UtcDateTime(2000),
        endTime: const UtcDateTime(2000).add(const Duration(hours: 2)),
        recurrenceRule: null,
      ),
      _TestAppointment(
        uuid: _TestAppointmentTypes.simpleRecurrence,
        startTime: const UtcDateTime(2000),
        endTime: const UtcDateTime(2000).add(const Duration(hours: 2)),
        recurrenceRule: 'FREQ=WEEKLY;BYDAY=MO;COUNT=5',
      ),

      /// One appointment changes the rrule every other reload.
      /// If the reloadCounter is even, it has one more occurence.
      _TestAppointment(
        uuid: _TestAppointmentTypes.changingCount,
        startTime: const UtcDateTime(2000),
        endTime: const UtcDateTime(2000).add(const Duration(hours: 2)),
        recurrenceRule: reloadCounter.isEven
            ? 'FREQ=WEEKLY;COUNT=1;BYDAY=MO'
            : 'FREQ=WEEKLY;COUNT=2;BYDAY=MO',
      ),
    ];

    return [
      ...testAppointments,

      /// One Appointment is there only every other reload.
      ...(reloadCounter.isEven
          ? [
              _TestAppointment(
                uuid: _TestAppointmentTypes.sometimesThere,
                startTime: const UtcDateTime(2000),
                endTime: const UtcDateTime(2000).add(const Duration(hours: 2)),
                recurrenceRule: 'FREQ=WEEKLY;COUNT=2;BYDAY=TU',
              ),
            ]
          : []),
    ];
  }

  @override
  _TestAppointment validateAppointment(
    final _TestAppointment appointment,
    final UtcDateTime date,
  ) =>
      appointment;
}

class _TestCalendarController extends EasyCalendarController<_TestAppointment> {
  @override
  EasyAppointmentDataSource<_TestAppointment> datasource;

  @override
  bool get shouldEnforceReload => true;

  _TestCalendarController({
    required this.datasource,
    required super.initialMinimumDate,
    required super.initialMaximumDate,
  });
}
