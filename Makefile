## basic make setup
.RECIPEPREFIX := > 
ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
# pretty arrow
M = $(shell if [ "$$(tput colors 2> /dev/null || echo 0)" -ge 8 ]; then printf "\033[34;1m▶\033[0m"; else printf "▶"; fi)

## directories
LIB_DIR := $(ROOT_DIR)lib

## commands
FLUTTER := $(shell which fvm > /dev/null && echo $$(which fvm) flutter || which flutter)
DART := $(shell which fvm > /dev/null && echo $$(which fvm) dart || which dart)
PUB=$(FLUTTER) pub

## make-targets
.PHONY: docs
docs: ; $(info $(M) creating docs for http://localhost:8080/…) @
> dart doc $(ROOT_DIR)
> dhttpd --path $(ROOT_DIR)doc/api
