## 0.1.2

- Make `agendaBorderSide` configurable

## 0.1.1

- Use [EasyTuple](https://pub.dev/packages/easy_tuple)

## 0.1.0

- Initial version.
