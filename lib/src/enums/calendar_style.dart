enum CalendarStyle {
  /// Is an (theoretically) endlessly scrollable list of
  /// things to be done - ordered by start time.
  ///
  /// You can style it by either overriding the methods used
  /// in the [EasyCalendarView] or by providing your own
  /// [AgendaViewSettings]\(recommended).
  agenda,

  /// Shows one day at a time, with all the tasks to do.
  scheduleDay,

  /// Shows one month at a time, with all the tasks to do.
  scheduleMonth,

  /// Shows one week at a time, with all the tasks to do.
  scheduleWeek,
}
