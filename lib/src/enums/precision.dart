enum Precision {
  day,
  hour,
  microsecond,
  millisecond,
  minute,
  month,
  second,
  year,
}
