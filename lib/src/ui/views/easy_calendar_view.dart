// ignore_for_file: avoid-unnecessary-stateful-widgets

import 'package:easy_calendar/easy_calendar.dart';
import 'package:easy_calendar/src/controllers/easy_calendar_controller.dart';
import 'package:easy_calendar/src/enums/calendar_style.dart';
import 'package:easy_calendar/src/enums/date_type.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/agenda_settings.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings/month_view_settings/day_cell_settings.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings/month_view_settings.dart';
import 'package:easy_calendar/src/ui/widgets/agenda_view/sliver_date_section_widget.dart';
import 'package:easy_calendar/src/ui/widgets/agenda_view_widget.dart';
import 'package:easy_calendar/src/ui/widgets/schedule_view_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:utc_date_time/utc_date_time.dart';

abstract class EasyCalendarView<TAppointmentData extends EasyAppointment>
    extends StatefulWidget {
  const EasyCalendarView({
    super.key,
    required this.settings,
    required this.controller,
    required this.style,
  });

  static const _totalWeeksInMonthView = 6;

  final EasyCalendarController<TAppointmentData> controller;

  final EasyCalendarSettings settings;

  final CalendarStyle style;

  AgendaSettings get agendaSettings => settings.agendaSettings;

  ScheduleSettings get scheduleSettings => settings.scheduleSettings;

  MonthViewSettings get monthViewSettings => scheduleSettings.monthViewSettings;

  DayCellSettings get dayCellSettings => monthViewSettings.dayCellSettings;

  @override
  State<EasyCalendarView<TAppointmentData>> createState() =>
      _EasyCalendarViewState<TAppointmentData>();

  @override
  void debugFillProperties(final DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty<EasyCalendarSettings>('settings', settings))
      ..add(
        DiagnosticsProperty<EasyCalendarController<TAppointmentData>>(
          'controller',
          controller,
        ),
      )
      ..add(EnumProperty<CalendarStyle>('style', style))
      ..add(
        DiagnosticsProperty<DayCellSettings>(
          'dayCellSettings',
          dayCellSettings,
        ),
      )
      ..add(
        DiagnosticsProperty<MonthViewSettings>(
          'monthViewSettings',
          monthViewSettings,
        ),
      )
      ..add(
        DiagnosticsProperty<ScheduleSettings>(
          'scheduleSettings',
          scheduleSettings,
        ),
      )
      ..add(
        DiagnosticsProperty<AgendaSettings>(
          'agendaSettings',
          agendaSettings,
        ),
      );
  }

  Widget buildAgendaView() {
    return AgendaViewWidget<TAppointmentData>(
      this,
      key: key,
      agendaSettings: agendaSettings,
    );
  }

  Widget buildScheduleView() {
    return ScheduleViewWidget<TAppointmentData>(
      this,
      key: key,
      scheduleSettings: scheduleSettings,
    );
  }

  SliverAppBar buildDateHeading(
    final BuildContext context,
    final UtcDateTime date,
  ) {
    final theme = Theme.of(context);

    return SliverAppBar(
      pinned: true,
      automaticallyImplyLeading: false,
      backgroundColor: theme.colorScheme.background,
      title: Text(date.todmY(), style: theme.textTheme.bodyLarge),
    );
  }

  Widget buildEmptyDatePlaceholder(
    final BuildContext context,
    final UtcDateTime date,
  ) =>
      Text('Nothing @ ${date.todmY()}');

  Widget buildItem(
    final BuildContext context,
    final TAppointmentData item,
    final UtcDateTime date,
  ) {
    final theme = Theme.of(context);

    return Card(
      // ignore: no-magic-number
      margin: const EdgeInsets.all(5),
      child: Container(
        // ignore: no-magic-number
        height: 80,
        alignment: Alignment.center,
        child: Text(
          '${item.subject} - ${item.startTime.todmYHM()}',
          style: theme.textTheme.bodyMedium?.copyWith(
            color: theme.colorScheme.onSecondary,
          ),
        ),
      ),
    );
  }

  Widget buildAgendaDate(final BuildContext context, final UtcDateTime date) {
    return SliverDateSectionWidget<TAppointmentData>(
      this,
      key: ValueKey<UtcDateTime>(date),
      date: date,
    );
  }

  /// Builds the schedule for the month/week/day.
  Widget buildScheduleDate(final BuildContext context, final UtcDateTime date) {
    switch (style) {
      case CalendarStyle.scheduleMonth:
        return buildScheduleMonthView(
          context,
          monthStart: date.monthStart,
          monthEnd: date.monthEnd,
        );

      case CalendarStyle.scheduleDay:
      case CalendarStyle.scheduleWeek:
        throw UnimplementedError('$style is not yet supported.');

      case CalendarStyle.agenda:
        throw UnimplementedError('CalendarStyle.agenda is incorrect here.');
    }
  }

  Widget buildScheduleMonthView(
    final BuildContext context, {
    required final UtcDateTime monthStart,
    required final UtcDateTime monthEnd,
  }) {
    Duration firstMondayOffset = Duration(days: monthStart.weekday - 1);

    /// We want to show the last week of the previous month, if the first day
    /// of the current month is a Monday. This looks better. Otherwise we have
    /// about 10 days after the current month and none before.
    /// To achieve this, we have to set the first Monday of the current Month
    /// view to be one week earlier/shift the beginning of the Month view back
    /// 7 more days.
    if (monthStart.weekday == UtcDateTime.monday) {
      firstMondayOffset += const Duration(days: UtcDateTime.daysPerWeek);
    }

    final firstMondayOfTheMonthView = monthStart.subtract(firstMondayOffset);

    final weekRows = <Widget>[];

    for (int weekCount = 0;
        weekCount < _totalWeeksInMonthView;
        weekCount += 1) {
      weekRows.add(
        buildWeekRow(
          context,
          mondayOfThatWeek: firstMondayOfTheMonthView.add(
            Duration(days: UtcDateTime.daysPerWeek * weekCount),
          ),
          monthStart: monthStart,
          monthEnd: monthEnd,
        ),
      );
    }

    return Column(
      children: [
        Padding(
          padding: monthViewSettings.headingPadding,
          child: Align(
            alignment: monthViewSettings.headingAlign,
            child: Text(
              DateFormat(
                monthViewSettings.headingDateFormat,
                Localizations.localeOf(context).languageCode,
              ).format(monthStart.dateTime),
              style: monthViewSettings.headingTextStyle ??
                  Theme.of(context).textTheme.headlineSmall,
            ),
          ),
        ),
        Flexible(
          child: buildWeekHeading(
            context,
            mondayDate: firstMondayOfTheMonthView,
          ),
        ),
        ...weekRows.map((final e) => Expanded(child: e)),
      ],
    );
  }

  Widget buildWeekHeading(
    final BuildContext context, {
    required final UtcDateTime mondayDate,
  }) {
    final formatter = DateFormat(
      monthViewSettings.weekDayFormat,
      Localizations.localeOf(context).languageCode,
    );

    return Row(
      children: List.generate(
        UtcDateTime.daysPerWeek,
        (final index) => Expanded(
          child: Text(
            formatter.format(mondayDate.add(Duration(days: index)).dateTime),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }

  Widget buildWeekRow(
    final BuildContext context, {
    required final UtcDateTime mondayOfThatWeek,
    required final UtcDateTime monthStart,
    required final UtcDateTime monthEnd,
  }) {
    final dayCircles = <Widget>[];

    final startDayDiff = mondayOfThatWeek.difference(monthStart).inDays;
    final endDayDiff = mondayOfThatWeek.difference(monthEnd).inDays;

    for (int daysSinceMonday = 0;
        daysSinceMonday < DateTime.daysPerWeek;
        daysSinceMonday += 1) {
      DateType dateType = DateType.currentMonth;

      if (daysSinceMonday > endDayDiff) {
        /// Date is after the actual month
        /// (but still part of the 6 shown weeks).
        if (!monthViewSettings.showAfterMonthDays) {
          dayCircles.add(Container());
          continue;
        }

        dateType = DateType.after;
      } else if (startDayDiff > daysSinceMonday) {
        /// Date is before the actual month
        /// (but still part of the 6 shown weeks).
        if (!monthViewSettings.showBeforeMonthDays) {
          dayCircles.add(Container());
          continue;
        }

        dateType = DateType.before;
      }
      final date = mondayOfThatWeek.add(Duration(days: daysSinceMonday));
      dayCircles.add(
        Expanded(
          child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () => controller.selectDate(date),
            child: buildDayCircle(
              context,
              date,
              dateType,
            ),
          ),
        ),
      );
    }

    return Row(children: dayCircles);
  }

  Widget buildDayCircle(
    final BuildContext context,
    final UtcDateTime date,
    final DateType dateType,
  ) {
    // print(
    //     'xx ${controller.selectedDate} == $date -> ${controller.selectedDate == date}');
    final isSelectedDate = controller.selectedDate.value == date;
    final isToday = date == UtcDateTime.now().dayStart;

    final textStyle = createDayCircleTextStyle(
      context,
      dateType: dateType,
      isSelectedDate: isSelectedDate,
      isToday: isToday,
    );

    final decoration = createDayCircleDecoration(
      context,
      dateType: dateType,
      isSelectedDate: isSelectedDate,
      isToday: isToday,
    );

    return Padding(
      padding: EdgeInsets.all(dayCellSettings.padding),
      child: Stack(
        children: [
          DecoratedBox(
            decoration: decoration,
            child: Align(child: Text(date.toDayString(), style: textStyle)),
          ),
          Positioned(
            bottom: 3,
            left: 0,
            right: 0,
            child: buildDotIndicatorRow(
              context,
              appointmentCount(
                context,
                date,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildDotIndicatorRow(
    final BuildContext context,
    final int count,
  ) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: Iterable<Widget?>.generate(
        count,
        (index) => buildDotIndicator(
          context,
          index,
          count,
        ),
      ).nonNulls.toList(),
    );
  }

  Widget? buildDotIndicator(
    final BuildContext context,
    final int index,
    final int totalCount,
  ) {
    if (index > 0) {
      return null;
    }
    return Container(
      width: 4,
      height: 4,
      margin: EdgeInsets.symmetric(horizontal: 1),
      decoration: ShapeDecoration(
        color: getDotIndicatorColor(context, index),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }

  Color getDotIndicatorColor(
    final BuildContext context,
    final int index,
  ) {
    return Theme.of(context).colorScheme.primary;
  }

  int appointmentCount(
    final BuildContext context,
    final UtcDateTime date,
  ) {
    return controller.getAppointmentsForDate(date).length;
  }

  TextStyle? createDayCircleTextStyle(
    final BuildContext context, {
    required final DateType dateType,
    required final bool isSelectedDate,
    required final bool isToday,
  }) {
    final appTheme = Theme.of(context);
    final defaultTextStyle =
        dayCellSettings.monthDayTextStyle ?? appTheme.textTheme.bodyLarge;

    final todayTextStyle = dayCellSettings.todayTextStyle == null
        ? defaultTextStyle?.copyWith(
            color:
                dayCellSettings.todayTextColor ?? appTheme.colorScheme.primary)
        : defaultTextStyle?.merge(dayCellSettings.todayTextStyle);

    if (isToday) {
      return todayTextStyle;
    }

    /// Default color for after and before day text.
    final defaultOutsideColor = appTheme.colorScheme.onBackground.withAlpha(96);

    switch (dateType) {
      case DateType.before:
        return dayCellSettings.beforeMonthDayTextStyle ??
            defaultTextStyle?.copyWith(
              color: dayCellSettings.beforeMonthDayTextColor ??
                  defaultOutsideColor,
            );

      case DateType.after:
        return dayCellSettings.afterMonthDayTextStyle ??
            defaultTextStyle?.copyWith(
              color:
                  dayCellSettings.afterMonthDayTextColor ?? defaultOutsideColor,
            );

      case DateType.currentMonth:
        return defaultTextStyle;
    }
  }

  Decoration createDayCircleDecoration(
    final BuildContext context, {
    required final DateType dateType,
    required final bool isSelectedDate,
    required final bool isToday,
  }) {
    return ShapeDecoration(
      shape: CircleBorder(
        side: BorderSide(
          width: getDayCircleBorderWidth(
            context,
            dateType: dateType,
            isSelectedDate: isSelectedDate,
            isToday: isToday,
          ),
        ).copyWith(
          color: getDayCircleBorderColor(
            context,
            dateType: dateType,
            isSelectedDate: isSelectedDate,
            isToday: isToday,
          ),
        ),
      ),
    );
  }

  double getDayCircleBorderWidth(
    final BuildContext context, {
    required final DateType dateType,
    required final bool isSelectedDate,
    required final bool isToday,
  }) {
    final defaultBorderWidth = dayCellSettings.borderWidth;
    final notTodayBorderWidth = isSelectedDate
        ? (dayCellSettings.selectionBorderWidth ?? defaultBorderWidth)
        : defaultBorderWidth;
    if (isToday) {
      return dayCellSettings.todayBorderWidth ?? notTodayBorderWidth;
    }
    return notTodayBorderWidth;
  }

  Color? getDayCircleBorderColor(
    final BuildContext context, {
    required final DateType dateType,
    required final bool isSelectedDate,
    required final bool isToday,
  }) {
    final defaultBorderColor =
        dayCellSettings.borderColor ?? Theme.of(context).colorScheme.secondary;
    final notTodayBorderColor = isSelectedDate
        ? (dayCellSettings.selectionBorderColor ?? defaultBorderColor)
        : defaultBorderColor;
    if (isToday) {
      return dayCellSettings.todayBorderColor ??
          Theme.of(context).colorScheme.primary;
    }
    return notTodayBorderColor;
  }

  Widget buildTextButton(
    final BuildContext context, {
    required final String text,
    required final VoidCallback onPressed,
  }) {
    return TextButton(onPressed: onPressed, child: Text(text));
  }

  Widget buildAgendaWrapperForMonthView(
    final BuildContext context,
    final Widget child,
  ) {
    final borderSide = monthViewSettings.agendaBorderSide.copyWith(
      strokeAlign: 1,
    );

    return Container(
      decoration: BoxDecoration(
        color: monthViewSettings.agendaBackgroundColor,
        borderRadius: monthViewSettings.agendaBorderRadius,
        border: Border(
          left: borderSide,
          top: borderSide,
          right: borderSide,
        ),
      ),
      child: ClipRRect(
        borderRadius: monthViewSettings.agendaBorderRadius,
        child: child,
      ),
    );
  }

  Widget buildAgendaForMonthView(
    final BuildContext context,
    final UtcDateTime date,
  ) {
    final appointmentsToShow = controller.appointmentsForSelectedDate;

    return appointmentsToShow.isEmpty
        ? buildEmptyDatePlaceholder(context, date)
        : ListView.builder(
            itemCount: appointmentsToShow.length,
            itemBuilder: (final ctx, final index) {
              final appointment = appointmentsToShow.elementAtOrNull(index);
              final validatedAppointment = appointment == null
                  ? null
                  : controller.datasource
                      .validateAppointment(appointment, date);

              return validatedAppointment == null
                  ? Container()
                  : buildItem(context, validatedAppointment, date);
            },
          );
  }
}

class _EasyCalendarViewState<TAppointmentData extends EasyAppointment>
    extends State<EasyCalendarView<TAppointmentData>> {
  @override
  Widget build(final BuildContext context) {
    switch (widget.style) {
      case CalendarStyle.scheduleDay:
      case CalendarStyle.scheduleWeek:
      case CalendarStyle.scheduleMonth:
        return widget.buildScheduleView();

      case CalendarStyle.agenda:
        return widget.buildAgendaView();
    }
  }
}
