// ignore_for_file: avoid-unnecessary-stateful-widgets

import 'package:easy_calendar/src/controllers/easy_calendar_controller.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings.dart';
import 'package:easy_calendar/src/ui/views/easy_calendar_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utc_date_time/utc_date_time.dart';

class ScheduleViewWidget<TAppointmentData extends EasyAppointment>
    extends StatefulWidget {
  const ScheduleViewWidget(
    this.parent, {
    required this.scheduleSettings,
    super.key,
    this.opacity = 1,
  });

  final EasyCalendarView<TAppointmentData> parent;

  final ScheduleSettings scheduleSettings;

  final double opacity;

  EasyCalendarController<TAppointmentData> get controller => parent.controller;

  @override
  State<ScheduleViewWidget<TAppointmentData>> createState() =>
      _ScheduleViewState();

  @override
  void debugFillProperties(final DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
        DiagnosticsProperty<EasyCalendarController<TAppointmentData>>(
          'controller',
          controller,
        ),
      )
      ..add(
        DiagnosticsProperty<ScheduleSettings>(
          'scheduleSettings',
          scheduleSettings,
        ),
      )
      ..add(DoubleProperty('opacity', opacity));
  }
}

class _ScheduleViewState<TAppointmentData extends EasyAppointment>
    extends State<ScheduleViewWidget<TAppointmentData>> {
  int get _startMonthsSinceZero =>
      _monthsSinceZero(widget.controller.absoluteMinimumDate.monthStart);

  int get _endMonthsSinceZero =>
      _monthsSinceZero(widget.controller.absoluteMaximumDate.monthStart);

  int get _currentMonthSinceZero =>
      _monthsSinceZero(widget.controller.selectedDateOrToday);

  int get _spannedMonths => _endMonthsSinceZero - _startMonthsSinceZero;

  int get _currentMonthIndex => _currentMonthSinceZero - _startMonthsSinceZero;

  // ignore: avoid-late-keyword
  late final PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: _currentMonthIndex,
      keepPage: !kDebugMode,
    );
  }

  int _monthsSinceZero(final UtcDateTime date) {
    return date.year * UtcDateTime.monthsPerYear + (date.month - 1);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: widget.controller.isRefreshing,
      builder: (final ctx, final value, final child) => ValueListenableBuilder(
        valueListenable: widget.controller.selectedDate,
        builder: (final ctx, final value, final child) {
          final currentDate = widget.controller.selectedDate.value;

          return Column(
            children: [
              Expanded(
                // ignore: no-magic-number
                flex: 100,
                child: PageView.builder(
                  itemCount: _spannedMonths,

                  /// Load last and next calendar page as well.
                  allowImplicitScrolling: true,
                  controller: _pageController,
                  itemBuilder: (final _, final itemIndex) {
                    final monthsSinceZero = _startMonthsSinceZero + itemIndex;

                    final monthStart = UtcDateTime(
                      monthsSinceZero ~/ UtcDateTime.monthsPerYear,
                      (monthsSinceZero % UtcDateTime.monthsPerYear) + 1,
                    );

                    if (monthStart.isBefore(widget.controller.minDate) &&
                        widget.controller.canLoadMoreBackwards) {
                      widget.controller
                          .loadMoreBackwards(untilAtLeast: monthStart);
                    }

                    if (monthStart.monthEnd
                            .isAfter(widget.controller.maxDate) &&
                        widget.controller.canLoadMoreForwards) {
                      widget.controller
                          .loadMoreForwards(untilAtLeast: monthStart.monthEnd);
                    }

                    return widget.parent.buildScheduleDate(context, monthStart);
                  },
                ),
              ),
              Expanded(
                flex: widget.parent.monthViewSettings.agendaSizing,
                child: widget.parent.buildAgendaWrapperForMonthView(
                  context,
                  SizedBox.expand(
                    child: currentDate == null
                        ? Container()
                        : widget.parent.buildAgendaForMonthView(
                            context,
                            currentDate,
                          ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
