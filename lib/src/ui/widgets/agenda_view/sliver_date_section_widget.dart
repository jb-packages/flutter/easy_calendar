import 'package:easy_calendar/src/extensions/utc_date_time_extension.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_calendar/src/ui/views/easy_calendar_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:super_sliver_list/super_sliver_list.dart';
import 'package:utc_date_time/utc_date_time.dart';

class SliverDateSectionWidget<TAppointmentData extends EasyAppointment>
    extends StatefulWidget {
  const SliverDateSectionWidget(
    this.parent, {
    super.key,
    required this.date,
  });

  final EasyCalendarView<TAppointmentData> parent;

  final UtcDateTime date;

  @override
  State<SliverDateSectionWidget> createState() =>
      _SliverDateSectionWidgetState();

  @override
  void debugFillProperties(final DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<UtcDateTime>('date', date));
  }
}

class _SliverDateSectionWidgetState<TAppointmentData extends EasyAppointment>
    extends State<SliverDateSectionWidget> {
  Iterable<TAppointmentData> _items = [];

  @override
  void initState() {
    super.initState();
    final controller = widget.parent.controller;

    _setItems();
    controller.addListenerForDate(_handleAppointmentsChange, widget.date);
  }

  void _setItems() {
    _items = widget.parent.controller
        .getAppointmentsForDate<TAppointmentData>(widget.date)
        .map(_validateAppointment)
        .nonNulls;
  }

  TAppointmentData? _validateAppointment(TAppointmentData appointment) {
    return widget.parent.controller.datasource
        .validateAppointment(appointment, widget.date) as TAppointmentData?;
  }

  void _handleAppointmentsChange() {
    setState(_setItems);
  }

  @override
  void dispose() {
    widget.parent.controller
        .removeListenerForDate(_handleAppointmentsChange, widget.date);
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    if (_items.isNotEmpty) {
      return SliverMainAxisGroup(
        slivers: [
          widget.parent.buildDateHeading(context, widget.date),
          SuperSliverList(
            delegate: SliverChildListDelegate.fixed(
              _items
                  .map(
                    (item) => widget.parent.buildItem(
                      context,
                      item,
                      widget.date,
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      );
    }
    // We build empty dates when we should either show all of the empty dates
    // no matter what or if we should always show today and the date is today.
    final shouldBuildEmptyDate = widget.parent.agendaSettings.showEmptyDates ||
        (widget.parent.agendaSettings.alwaysShowToday && widget.date.isToday());

    if (_items.isEmpty && shouldBuildEmptyDate) {
      return SliverMainAxisGroup(
        slivers: [
          widget.parent.buildDateHeading(context, widget.date),
          SliverToBoxAdapter(
            child:
                widget.parent.buildEmptyDatePlaceholder(context, widget.date),
          )
        ],
      );
    }

    return SliverToBoxAdapter(child: Container());
  }
}
