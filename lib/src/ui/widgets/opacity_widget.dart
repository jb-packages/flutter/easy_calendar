import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class OpacityWidget extends StatelessWidget {
  const OpacityWidget({
    this.animationDuration = 1,
    required this.child,
    super.key,
    required this.opacity,
  });

  static const _pointerIgnoreThreshold = 0.1;

  final double opacity;

  final int animationDuration;

  final Widget child;

  @override
  void debugFillProperties(final DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DoubleProperty('opacity', opacity))
      ..add(IntProperty('animationDuration', animationDuration));
  }

  @override
  Widget build(final BuildContext context) {
    return AnimatedOpacity(
      opacity: opacity,
      duration: Duration(seconds: animationDuration),
      child: IgnorePointer(
        ignoring: opacity < _pointerIgnoreThreshold,
        child: child,
      ),
    );
  }
}
