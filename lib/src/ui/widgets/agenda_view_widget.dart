import 'package:easy_calendar/src/controllers/easy_calendar_controller.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/agenda_settings.dart';
import 'package:easy_calendar/src/ui/views/easy_calendar_view.dart';
import 'package:easy_calendar/src/ui/widgets/opacity_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utc_date_time/utc_date_time.dart';

class AgendaViewWidget<TAppointmentData extends EasyAppointment>
    extends StatefulWidget {
  const AgendaViewWidget(
    this.parent, {
    required this.agendaSettings,
    super.key,
    this.opacity = 1,
  });

  final EasyCalendarView<TAppointmentData> parent;

  final AgendaSettings agendaSettings;

  final double opacity;

  EasyCalendarController<TAppointmentData> get controller => parent.controller;

  @override
  State<AgendaViewWidget<TAppointmentData>> createState() =>
      _AgendaViewWidgetState();

  @override
  void debugFillProperties(final DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(
        DiagnosticsProperty<AgendaSettings>('agendaSettings', agendaSettings),
      )
      ..add(DoubleProperty('opacity', opacity))
      ..add(
        DiagnosticsProperty<EasyCalendarController<TAppointmentData>>(
          'controller',
          controller,
        ),
      );
  }
}

class _AgendaViewWidgetState<TAppointmentData extends EasyAppointment>
    extends State<AgendaViewWidget<TAppointmentData>> {
  List<UtcDateTime> _previousDates = [];
  List<UtcDateTime> _nextDates = [];

  final _agendaScrollViewKey = UniqueKey();
  final _agendaViewKey = UniqueKey();

  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_refresh);
    _setDates();
  }

  void _setDates() {
    _previousDates = widget.controller.previousDates.toList();
    _nextDates = widget.controller.nextDates.toList();
  }

  void _refresh() {
    setState(_setDates);
  }

  @override
  void dispose() {
    widget.controller.removeListener(_refresh);
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    // if (kDebugMode) {
    //   print('_previousDates: $_previousDates');
    // }
    final agendaScrollView = CustomScrollView(
      key: _agendaScrollViewKey,
      physics: const AlwaysScrollableScrollPhysics(
        parent: ClampingScrollPhysics(
          parent: RangeMaintainingScrollPhysics(),
        ),
      ),
      controller: _scrollController,
      center: _agendaViewKey,
      slivers: [
        SliverToBoxAdapter(
          child: ValueListenableBuilder(
              valueListenable: widget.controller.isRefreshing,
              builder: (context, isRefreshing, _) {
                if (widget.agendaSettings.mayLoadMoreBackwards &&
                    widget.controller.canLoadMoreBackwards) {
                  return widget.parent.buildTextButton(
                    context,
                    onPressed: () => widget.controller.loadMoreBackwards(),
                    text: 'Load until ${widget.controller.previousChunkStart}',
                  );
                }
                return Container();
              }),
        ),
        ..._previousDates.map(
          (final dateToBuild) => Container(
            key: ValueKey<UtcDateTime>(dateToBuild),
            child: widget.parent.buildAgendaDate(context, dateToBuild),
          ),
        ),

        /// Always build current date, because this is
        /// the center of the ScrollView.
        Container(
          key: _agendaViewKey,
          child: widget.parent.buildAgendaDate(
            context,
            UtcDateTime.now().dayStart,
          ),
        ),

        ..._nextDates.map(
          (final dateToBuild) => Container(
            key: ValueKey<UtcDateTime>(dateToBuild),
            child: widget.parent.buildAgendaDate(context, dateToBuild),
          ),
        ),
        SliverToBoxAdapter(
          child: ValueListenableBuilder(
            valueListenable: widget.controller.isRefreshing,
            builder: (context, isRefreshing, _) {
              if (widget.agendaSettings.mayLoadMoreForwards &&
                  widget.controller.canLoadMoreForwards) {
                return widget.parent.buildTextButton(
                  context,
                  onPressed: () => widget.controller.loadMoreForwards(),
                  text: 'Load until ${widget.controller.nextChunkEnd}',
                );
              }
              return Container();
            },
          ),
        ),
      ],
    );

    return Stack(
      children: [
        OpacityWidget(opacity: widget.opacity, child: agendaScrollView),
      ],
    );
  }
}
