// ignore_for_file: format-comment

// WIP
// import 'package:flutter/material.dart';

// class EasyCalendarTheme extends InheritedWidget {
//   const EasyCalendarTheme({super.key, required super.child});

//   factory EasyCalendarTheme.fallback({final Key? key}) {
//     return EasyCalendarTheme(key: key, child: const _NullWidget());
//   }

//   factory EasyCalendarTheme.of(final BuildContext context) {
//     return context.dependOnInheritedWidgetOfExactType<EasyCalendarTheme>() ??
//         EasyCalendarTheme.fallback();
//   }

//   @override
//   bool updateShouldNotify(final EasyCalendarTheme oldWidget) =>
//       oldWidget != this;
// }

// // ignore: prefer-single-widget-per-file
// class _NullWidget extends StatelessWidget {
//   const _NullWidget();

//   @override
//   Widget build(final BuildContext context) {
//     throw FlutterError(
//       'A EasyCalendarTheme constructed with EasyCalendarTheme.fallback cannot '
//       'be incorporated into the widget tree, it is meant only to provide a '
//       'fallback value returned by EasyCalendarTheme.of() when no enclosing '
//       'default selection style is present in a BuildContext.',
//     );
//   }
// }
