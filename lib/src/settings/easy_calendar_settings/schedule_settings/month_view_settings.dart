import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings/month_view_settings/day_cell_settings.dart';
import 'package:flutter/material.dart';

/// Used to change settings for ScheduleView with
/// `style: CalendarStyle.scheduleMonth`.
class MonthViewSettings {
  final DayCellSettings dayCellSettings;

  final bool showBeforeMonthDays;
  final bool showAfterMonthDays;

  final String headingDateFormat;
  final String weekDayFormat;

  final TextStyle? headingTextStyle;

  final EdgeInsets agendaPadding;
  final EdgeInsets headingPadding;

  final AlignmentGeometry headingAlign;

  /// This parameter specifies the height of the agenda below the month
  /// "in percent", in relation to the height of all weeks combined.
  final int agendaSizing;

  final Color agendaBackgroundColor;

  final BorderRadius agendaBorderRadius;
  final BorderSide agendaBorderSide;

  const MonthViewSettings({
    this.showBeforeMonthDays = true,
    this.showAfterMonthDays = true,
    this.headingDateFormat = 'MMMM y',
    this.weekDayFormat = 'E',
    this.headingTextStyle,
    this.agendaPadding = const EdgeInsets.all(0),
    this.headingPadding = const EdgeInsets.all(8),
    this.headingAlign = Alignment.centerLeft,
    this.dayCellSettings = const DayCellSettings(),
    // ignore: no-magic-number
    this.agendaSizing = 80,
    this.agendaBackgroundColor = Colors.transparent,
    this.agendaBorderRadius = BorderRadius.zero,
    this.agendaBorderSide = BorderSide.none,
  });
}
