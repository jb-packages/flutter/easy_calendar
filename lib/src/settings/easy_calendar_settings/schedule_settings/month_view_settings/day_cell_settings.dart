import 'package:flutter/material.dart';

class DayCellSettings {
  final double padding;
  final double borderWidth;
  final double? selectionBorderWidth;
  final double? todayBorderWidth;

  final Color? borderColor;
  final Color? selectionBorderColor;
  final Color? todayBorderColor;
  final Color? todayTextColor;
  final Color? beforeMonthDayTextColor;
  final Color? afterMonthDayTextColor;

  final TextStyle? monthDayTextStyle;
  final TextStyle? todayTextStyle;
  final TextStyle? beforeMonthDayTextStyle;
  final TextStyle? afterMonthDayTextStyle;

  const DayCellSettings({
    // ignore: no-magic-number
    this.padding = 4.0,
    this.borderWidth = 0.5,
    this.selectionBorderWidth,
    this.todayBorderWidth,
    this.borderColor,
    this.selectionBorderColor,
    this.todayBorderColor,
    this.todayTextColor,
    this.afterMonthDayTextColor,
    this.beforeMonthDayTextColor,
    this.monthDayTextStyle,
    this.todayTextStyle,
    this.beforeMonthDayTextStyle,
    this.afterMonthDayTextStyle,
  });
}
