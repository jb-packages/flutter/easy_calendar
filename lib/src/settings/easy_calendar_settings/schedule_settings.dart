import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings/month_view_settings.dart';
import 'package:flutter/material.dart';

@immutable
class ScheduleSettings {
  final MonthViewSettings monthViewSettings;

  const ScheduleSettings({
    this.monthViewSettings = const MonthViewSettings(),
  });
}
