import 'package:flutter/foundation.dart';

@immutable
class AgendaSettings {
  final bool mayLoadMoreBackwards;
  final bool mayLoadMoreForwards;
  final bool showEmptyDates;
  final bool alwaysShowToday;

  const AgendaSettings({
    this.mayLoadMoreBackwards = false,
    this.mayLoadMoreForwards = true,
    this.showEmptyDates = false,
    this.alwaysShowToday = true,
  });
}
