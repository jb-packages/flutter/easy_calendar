import 'package:easy_calendar/src/settings/easy_calendar_settings/agenda_settings.dart';
import 'package:easy_calendar/src/settings/easy_calendar_settings/schedule_settings.dart';
import 'package:flutter/foundation.dart';

@immutable
class EasyCalendarSettings {
  final AgendaSettings agendaSettings;
  final ScheduleSettings scheduleSettings;

  const EasyCalendarSettings({
    this.agendaSettings = const AgendaSettings(),
    this.scheduleSettings = const ScheduleSettings(),
  });
}
