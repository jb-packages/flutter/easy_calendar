import 'dart:async';

// ignore: avoid-dynamic, prefer_void_to_null
typedef OnErrorCallback = FutureOr<Null> Function(dynamic error);
