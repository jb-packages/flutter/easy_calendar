import 'package:easy_calendar/src/enums/precision.dart';
import 'package:utc_date_time/utc_date_time.dart';

extension UtcDateTimeExtension on UtcDateTime {
  /// Check the dates are equal or not.
  /// [precision] determines up to which point the dates should have the same
  /// values to be considered equal.
  bool isSame(final UtcDateTime date, {final Precision? precision}) {
    bool isSame = true;
    switch (precision) {
      case null: // Setting the "null" case, sets the default.
      case Precision.microsecond:
        isSame = isSame && microsecond == date.microsecond;
        continue millisecond;

      millisecond:
      case Precision.millisecond:
        isSame = isSame && millisecond == date.millisecond;
        continue second;

      second:
      case Precision.second:
        isSame = isSame && second == date.second;
        continue minute;

      minute:
      case Precision.minute:
        isSame = isSame && minute == date.minute;
        continue hour;

      hour:
      case Precision.hour:
        isSame = isSame && hour == date.hour;
        continue day;

      day:
      case Precision.day:
        isSame = isSame && day == date.day;
        continue month;

      month:
      case Precision.month:
        isSame = isSame && month == date.month;
        continue year;

      year:
      case Precision.year:
        isSame = isSame && year == date.year;
    }

    return isSame;
  }

  /// Check if the date is today.
  bool isToday() {
    return isSame(
      UtcDateTime.now(),
      precision: Precision.day,
    );
  }

  /// Check the date before/same of last date.
  bool isSameOrBefore(final UtcDateTime date, {final Precision? precision}) {
    return isSame(date, precision: precision) || isBefore(date);
  }

  /// Check the date after/same of first date.
  bool isSameOrAfter(final UtcDateTime date, {final Precision? precision}) {
    return isSame(date, precision: precision) || isAfter(date);
  }

  bool isWithinRange(
    final UtcDateTime startDate,
    final UtcDateTime endDate, {
    final Precision? precision,
  }) {
    return (startDate.isAfter(endDate))
        // ignore: avoid-recursive-calls
        ? isWithinRange(endDate, startDate, precision: precision)
        : (isSameOrBefore(endDate, precision: precision) &&
            isSameOrAfter(startDate, precision: precision));
  }

  UtcDateTime clamp(final UtcDateTime minDate, final UtcDateTime maxDate) {
    return isBefore(minDate)
        ? minDate
        : (isAfter(maxDate) ? maxDate : copyWith());
  }

  /// Returns UtcTime with day precision; accepts negative dayCount.
  UtcDateTime diffDays(final int dayCount) {
    final action = dayCount < 0 ? subtract : add;

    return action(Duration(days: dayCount.abs()));
  }
}
