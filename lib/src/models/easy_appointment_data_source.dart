import 'dart:async';

import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_tuple/easy_tuple.dart';
import 'package:utc_date_time/utc_date_time.dart';

abstract class EasyAppointmentDataSource<
    TAppointmentData extends EasyAppointment> {
  List<TAppointmentData> _appointments = [];

  EasyAppointmentDataSource() {
    final _ = getAppointments(shouldReload: true);
  }

  /// If this returns null, the recurrence will not appear in the calendar.
  TAppointmentData? validateAppointment(
    final TAppointmentData appointment,
    final UtcDateTime date,
  );

  FutureOr<List<TAppointmentData>> loadAppointments();

  EasyTuple<List<TAppointmentData>> getAppointments({
    final bool shouldReload = false,
  }) {
    if (shouldReload) {
      // ignore: discarded_futures
      final appointmentsOrFuture = loadAppointments();

      if (appointmentsOrFuture is Future) {
        return EasyTuple(
          _appointments,
          isLoading: true,
          reloader: () async {
            // ignore: prefer-unwrapping-future-or
            return _appointments = await appointmentsOrFuture;
          },
        );
      }
      _appointments = appointmentsOrFuture;
    }

    return EasyTuple(_appointments, isLoading: false);
  }
}
