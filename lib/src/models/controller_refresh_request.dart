import 'package:utc_date_time/utc_date_time.dart';

/// Used to queue refresh requests better
class ControllerRefreshRequest {
  final UtcDateTime minDate;
  final UtcDateTime maxDate;

  const ControllerRefreshRequest({
    required this.minDate,
    required this.maxDate,
  });
}
