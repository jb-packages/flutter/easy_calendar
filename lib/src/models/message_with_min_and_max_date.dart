import 'package:utc_date_time/utc_date_time.dart';

/// Used to pass data and min and max
/// date from and to Isolates more easily.
class MessageWithMinAndMaxDate<T> {
  final T data;
  final T moreData;
  final UtcDateTime minDate;
  final UtcDateTime maxDate;

  const MessageWithMinAndMaxDate(
    this.data,
    this.moreData, {
    required this.minDate,
    required this.maxDate,
  });
}
