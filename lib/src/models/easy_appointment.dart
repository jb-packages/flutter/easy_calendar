import 'package:easy_calendar/src/ui/views/easy_calendar_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:utc_date_time/utc_date_time.dart';

@immutable
abstract class EasyAppointment {
  /// Delete the occurrence for an recurrence appointment.
  ///
  /// Defaults to `null`.
  List<DateTime>? get recurrenceExceptionDates => null;

  /// Defines the location for an [EasyAppointment] in [EasyCalendarView].
  ///
  /// Defaults to null.
  String? get location => null;

  /// The start time for an [EasyAppointment] in [EasyCalendarView].
  UtcDateTime get startTime;

  /// The end time for an [EasyAppointment] in [EasyCalendarView].
  UtcDateTime get endTime;

  /// Displays the [EasyAppointment] on the all day panel area of time slot
  /// views in [EasyCalendarView].
  ///
  /// Defaults to `false`.
  bool get isAllDay => false;

  /// RecurrenceRule for the [EasyAppointment] on [EasyCalendarView].
  ///
  /// Defaults to null.
  String? get recurrenceRule => null;

  /// The color that fills the background of the [EasyAppointment] view in
  /// [EasyCalendarView].
  ///
  /// Defaults to null.
  ///
  /// By default the EasyCalendarView calculates a color for an event.
  /// Override this getter to override the color the event will have in
  /// any EasyCalendarView.
  Color? get color => null;

  /// The subject for the [EasyAppointment] in [EasyCalendarView].
  String get subject;

  /// Defines the notes for an [EasyAppointment] in [EasyCalendarView].
  ///
  /// Defaults to null.
  String? get notes => null;

  /// Defines the id for [EasyAppointment] in [EasyCalendarView].
  String get uuid;

  /// Defines the appointment is spanned appointment or not.
  /// TODO(DrBu7cher): make it do something.
  bool get isSpanned => false;

  @override
  int get hashCode {
    return Object.hash(
      recurrenceRule,
      isAllDay,
      notes,
      location,
      uuid,
      startTime,
      endTime,
      subject,
      color,
      Object.hashAll(recurrenceExceptionDates ?? []),
    );
  }

  const EasyAppointment();

  @override
  bool operator ==(final Object other) {
    return identical(this, other) ||
        ((other is EasyAppointment) &&
            other.startTime == startTime &&
            other.endTime == endTime &&
            other.isSpanned == isSpanned &&
            other.isAllDay == isAllDay &&
            other.notes == notes &&
            other.location == location &&
            other.uuid == uuid &&
            other.subject == subject &&
            other.color == color &&
            other.recurrenceRule == recurrenceRule &&
            listEquals(
              other.recurrenceExceptionDates,
              recurrenceExceptionDates,
            ));
  }
}
