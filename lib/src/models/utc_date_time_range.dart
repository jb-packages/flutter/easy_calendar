import 'package:utc_date_time/utc_date_time.dart';

/// Is basically a [DateTimeRange], but for [UtcDateTime].
class UtcDateTimeRange {
  /// The start of the range of dates.
  final UtcDateTime start;

  /// The end of the range of dates.
  final UtcDateTime end;

  /// Returns a [Duration] of the time between [start] and [end].
  ///
  /// See [UtcDateTime.difference] for more details.
  Duration get duration => end.difference(start);

  @override
  int get hashCode => Object.hash(start, end);

  /// Creates a date range for the given start and end [DateTime].
  UtcDateTimeRange({required this.start, required this.end})
      : assert(!start.isAfter(end));

  @override
  bool operator ==(Object other) {
    return other.runtimeType == runtimeType &&
        other is UtcDateTimeRange &&
        other.start == start &&
        other.end == end;
  }

  @override
  String toString() => '$start - $end';
}
