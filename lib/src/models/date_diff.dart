// ignore_for_file: avoid-unsafe-collection-methods

import 'package:diffutil_dart/diffutil.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:flutter/foundation.dart';

@immutable
class DateDiff<TAppointmentData extends EasyAppointment>
    extends ListDiffDelegate<MapEntry<int, List<TAppointmentData>>> {
  DateDiff({
    required final Iterable<MapEntry<int, List<TAppointmentData>>> oldList,
    required final Iterable<MapEntry<int, List<TAppointmentData>>> newList,
  }) : super(
          oldList.toList()..sort((final a, final b) => a.key.compareTo(b.key)),
          newList.toList()..sort((final a, final b) => a.key.compareTo(b.key)),
        );

  @override
  bool areContentsTheSame(
    final int oldItemPosition,
    final int newItemPosition,
  ) {
    return listEquals(
      oldList[oldItemPosition].value,
      newList[newItemPosition].value,
    );
  }

  @override
  bool areItemsTheSame(final int oldItemPosition, final int newItemPosition) {
    return oldList[oldItemPosition].key == newList[newItemPosition].key;
  }
}
