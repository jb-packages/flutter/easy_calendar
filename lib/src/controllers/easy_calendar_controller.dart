import 'dart:async';

import 'package:diffutil_dart/diffutil.dart';
import 'package:easy_calendar/src/callbacks/on_error_callback.dart';
import 'package:easy_calendar/src/enums/precision.dart';
import 'package:easy_calendar/src/extensions/utc_date_time_extension.dart';
import 'package:easy_calendar/src/models/controller_refresh_request.dart';
import 'package:easy_calendar/src/models/date_diff.dart';
import 'package:easy_calendar/src/models/easy_appointment.dart';
import 'package:easy_calendar/src/models/easy_appointment_data_source.dart';
import 'package:easy_calendar/src/models/message_with_min_and_max_date.dart';
import 'package:flutter/foundation.dart';
import 'package:rrule/rrule.dart';
import 'package:utc_date_time/utc_date_time.dart';

abstract class EasyCalendarController<TAppointmentData extends EasyAppointment>
    with ChangeNotifier {
  final Duration chunkSize;

  final UtcDateTime absoluteMinimumDate;
  final UtcDateTime absoluteMaximumDate;

  final OnErrorCallback? _onError;

  UtcDateTime _minDate;
  UtcDateTime _maxDate;

  final _isLoadingBackwards = ValueNotifier<bool>(false);
  final _isLoadingForwards = ValueNotifier<bool>(false);
  final _isRefreshing = ValueNotifier<bool>(false);

  static const _daysInMonthCount = 30;

  Iterable<UtcDateTime> _previousDates = [];
  Iterable<UtcDateTime> _nextDates = [];

  final _appointmentsPerMillis = <int, ValueNotifier<List<TAppointmentData>>>{};

  // ignore: prefer_final_fields
  ValueNotifier<UtcDateTime?> _selectedDate;

  UtcDateTime get minDate => _minDate;
  UtcDateTime get maxDate => _maxDate;

  EasyAppointmentDataSource<TAppointmentData> get datasource;

  List<TAppointmentData> get appointments => datasource.getAppointments().item;
  ValueListenable<bool> get isRefreshing => _isRefreshing;

  bool get canLoadMoreBackwards => absoluteMinimumDate.isBefore(_minDate);
  UtcDateTime get previousChunkStart => _minDate.subtract(chunkSize);
  Iterable<UtcDateTime> get previousDates => _previousDates;

  bool get canLoadMoreForwards => absoluteMinimumDate.isBefore(_minDate);
  UtcDateTime get nextChunkEnd => _maxDate.add(chunkSize);
  Iterable<UtcDateTime> get nextDates => _nextDates;

  bool get shouldEnforceReload;

  List<TAppointmentData> get appointmentsForSelectedDate {
    // ignore: avoid-non-null-assertion
    return selectedDate.value == null
        ? []
        : getAppointmentsForDate(selectedDate.value!);
  }

  ValueListenable<bool> get isLoadingBackwards => _isLoadingBackwards;
  ValueListenable<bool> get isLoadingForwards => _isLoadingForwards;

  ValueListenable<UtcDateTime?> get selectedDate => _selectedDate;

  UtcDateTime get selectedDateOrToday =>
      _selectedDate.value ?? UtcDateTime.now().dayStart;

  /// An [EasyCalendarController] is used to manage all calendar data.
  ///
  /// [minDate] and [maxDate] of the [EasyCalendarController] can only
  /// be directly set upon initializing it by using [initialMinimumDate]
  /// and [initialMaximumDate].
  /// They can later be changed in steps of [chunkSize]
  /// with [loadMoreBackwards] and [loadMoreForwards] respectively.
  /// They are clamped by [absoluteMinimumDate] and [absoluteMaximumDate].
  EasyCalendarController({
    final OnErrorCallback? onError,
    final UtcDateTime? initialMinimumDate,
    this.chunkSize = const Duration(days: _daysInMonthCount),
    // ignore: avoid-similar-names
    final UtcDateTime? initialMaximumDate,
    // ignore: no-magic-number
    this.absoluteMinimumDate = const UtcDateTime(2000),
    // ignore: avoid-similar-names, no-magic-number
    this.absoluteMaximumDate = const UtcDateTime(3000),
    final UtcDateTime? initialSelectedDate,
  })  : assert(chunkSize.inDays > 0, 'chunkSize must be at least one day'),
        _selectedDate = ValueNotifier(initialSelectedDate?.dayStart),
        _minDate = initialMinimumDate ?? UtcDateTime.now().dayStart,
        _maxDate = initialMaximumDate ??
            (initialMinimumDate ?? UtcDateTime.now().dayStart).add(chunkSize),
        _onError = onError {
    unawaited(refresh().catchError(this.onError));
  }

  // ignore: avoid-dynamic, prefer_void_to_null
  FutureOr<Null> onError(final dynamic err) {
    if (_onError != null) {
      return _onError(err);
    }
    if (kDebugMode) {
      print('An error occurred: $err');
    }
  }

  Future<void> refresh() {
    return _refresh(refreshMinDate: _minDate, refreshMaxDate: _maxDate);
  }

  List<T> getAppointmentsForDate<T extends EasyAppointment>(
    final UtcDateTime date,
  ) {
    return _notifierForDate(date).value as List<T>;
  }

  void addListenerForDate(
    final VoidCallback listener,
    final UtcDateTime date,
  ) {
    // if (kDebugMode) {
    //   print('adding listener for $date');
    // }

    _notifierForDate(date).addListener(listener);
  }

  void removeListenerForDate(
    final VoidCallback listener,
    final UtcDateTime date,
  ) {
    // if (kDebugMode) {
    //   print('removing listener from $date');
    // }
    final notifier = _notifierForDate(date)..removeListener(listener);

    if (notifier.value.isEmpty && !notifier.hasListeners) {
      _removeNotifierForDate(date);
    }
  }

  @override
  @mustCallSuper
  void dispose() {
    _isLoadingBackwards.dispose();
    _isLoadingForwards.dispose();
    super.dispose();
  }

  void loadMoreBackwards({final UtcDateTime? untilAtLeast}) {
    // We should not reload if there is already a reload in progress.
    if (_isLoadingBackwards.value) {
      return;
    }

    if (_minDate.isSame(absoluteMinimumDate, precision: Precision.day)) {
      // We should not load more.
      return;
    }

    _isLoadingBackwards.value = true;

    UtcDateTime newMinDate = _minDate.subtract(chunkSize);

    while (untilAtLeast != null && newMinDate.isAfter(untilAtLeast)) {
      newMinDate = newMinDate.subtract(chunkSize);
    }

    if (newMinDate.isSameOrBefore(
      absoluteMinimumDate,
      precision: Precision.day,
    )) {
      newMinDate = absoluteMinimumDate;
    }

    unawaited(
      _refresh(
        refreshMinDate: newMinDate,
        refreshMaxDate: _minDate.diffDays(-1).dayEnd,
      )
          .then((final value) {
            _minDate = newMinDate;
          })
          .whenComplete(() => _isLoadingBackwards.value = false)
          .catchError(onError),
    );
  }

  void loadMoreForwards({final UtcDateTime? untilAtLeast}) {
    // We should not reload if there is already a reload in progress.
    if (_isLoadingForwards.value) {
      return;
    }

    if (_maxDate.isSame(absoluteMaximumDate, precision: Precision.day)) {
      // We should not load more.
      return;
    }

    _isLoadingForwards.value = true;

    UtcDateTime newMaxDate = _maxDate.add(chunkSize);

    while (untilAtLeast != null && newMaxDate.isBefore(untilAtLeast)) {
      newMaxDate = newMaxDate.add(chunkSize);
    }

    if (newMaxDate.isSameOrAfter(
      absoluteMaximumDate,
      precision: Precision.day,
    )) {
      newMaxDate = absoluteMaximumDate;
    }

    unawaited(
      _refresh(
        refreshMinDate: _maxDate.diffDays(1).dayStart,
        refreshMaxDate: newMaxDate,
      )
          .then((final value) {
            _maxDate = newMaxDate;
          })
          .whenComplete(() => _isLoadingForwards.value = false)
          .catchError(onError),
    );
  }

  void handleDiff(
    final DiffResult<MapEntry<int, List<TAppointmentData>>> uuidDiff,
  ) {
    for (final diff in uuidDiff.getUpdatesWithData()) {
      diff.when(
        insert: (final position, final appointment) {
          // if (kDebugMode) {
          //   print('insert: $appointment');
          // }
          _addAppointment(appointment);
        },
        remove: (final position, final appointment) {
          // if (kDebugMode) {
          //   print('remove: $appointment');
          // }
          _removeAppointment(appointment);
        },
        change: (final position, final oldAppointment, final newAppointment) {
          // if (kDebugMode) {
          //   print('oldAppointment: $oldAppointment');
          //   print('newAppointment: $newAppointment');
          // }
          // _removeAppointment(oldAppointment);.
          _addAppointment(newAppointment);
        },
        move: (final from, final to, final appointment) {
          // if (kDebugMode) {
          //   print('move: $appointment - from $from -> to $to');
          // }
        },
      );
    }
  }

  // ignore: use_setters_to_change_properties
  void selectDate(final UtcDateTime date) {
    print('xx selectDate $date');
    _selectedDate.value = date;
  }

  static DiffResult<MapEntry<int, List<TAppointmentData>>>
      _getDiff<TAppointmentData extends EasyAppointment>(
    final MessageWithMinAndMaxDate<List<MapEntry<int, List<TAppointmentData>>>>
        message,
  ) {
    final currentAppointmentsPerMillis = message.data;
    final newlyLoadedAppointments = message.moreData;
    // print("newlyLoadedAppointments: $newlyLoadedAppointments");

    return calculateDiff<MapEntry<int, List<TAppointmentData>>>(
      DateDiff<TAppointmentData>(
        oldList: currentAppointmentsPerMillis,
        newList: newlyLoadedAppointments,
      ),
    );
  }

  void _removeNotifierForDate(final UtcDateTime date) =>
      _removeNotifierFor(_keyFor(date));

  void _removeNotifierFor(final int dateKey) {
    _appointmentsPerMillis[dateKey]?.dispose();

    final _ = _appointmentsPerMillis.remove(dateKey);
  }

  ValueNotifier<List<TAppointmentData>> _notifierForDate(
    final UtcDateTime date,
  ) =>
      _notifierFor(_keyFor(date));

  ValueNotifier<List<TAppointmentData>> _notifierFor(final int dateKey) =>
      _appointmentsPerMillis[dateKey] ??= ValueNotifier([]);

  List<ControllerRefreshRequest> _refreshRequests = [];

  Future<void> _refresh({
    required final UtcDateTime refreshMinDate,
    required final UtcDateTime refreshMaxDate,
  }) async {
    if (_isRefreshing.value) {
      print('adding refresh request - $refreshMinDate to $refreshMaxDate');
      _refreshRequests.add(
        ControllerRefreshRequest(
          minDate: refreshMinDate,
          maxDate: refreshMaxDate,
        ),
      );
      return;
    }

    _isRefreshing.value = true;
    final appointmentGetterTuple =
        datasource.getAppointments(shouldReload: shouldEnforceReload);
    final newAppointments = List.of(await appointmentGetterTuple.reloader);
    if (kDebugMode) {
      print('_refresh: $refreshMinDate -> $refreshMaxDate: $newAppointments');
    }

    final newAppointmentsPerMillis = await compute<
        MessageWithMinAndMaxDate<List<TAppointmentData>>,
        Map<int, List<TAppointmentData>>>(
      _getAppointmentsPerMillis<TAppointmentData>,
      MessageWithMinAndMaxDate(
        newAppointments,
        [],
        minDate: refreshMinDate,
        maxDate: refreshMaxDate,
      ),
      debugLabel: 'newAppointmentsPerMillis',
    );

    final minMillis = refreshMinDate.millisecondsSinceEpoch;
    final maxMillis = refreshMaxDate.millisecondsSinceEpoch;

    final uuidDiff = await compute<
        MessageWithMinAndMaxDate<List<MapEntry<int, List<TAppointmentData>>>>,
        DiffResult<MapEntry<int, List<TAppointmentData>>>>(
      _getDiff<TAppointmentData>,
      MessageWithMinAndMaxDate(
        _appointmentsPerMillis.entries
            .where(
              (final apEntry) =>
                  apEntry.key >= minMillis && apEntry.key <= maxMillis,
            )
            .map((final apEntry) => MapEntry(apEntry.key, apEntry.value.value))
            .toList(),
        newAppointmentsPerMillis.entries.toList(),
        minDate: refreshMinDate,
        maxDate: refreshMaxDate,
      ),
      debugLabel: 'uuidDiff',
    );

    handleDiff(uuidDiff);

    _previousDates = _newPreviousDates(_appointmentsPerMillis);
    _nextDates = _newNextDates(_appointmentsPerMillis);

    _isRefreshing.value = false;
    notifyListeners();

    if (_refreshRequests.isNotEmpty) {
      final request = _refreshRequests.removeAt(0);
      await _refresh(
        refreshMinDate: request.minDate,
        refreshMaxDate: request.maxDate,
      );
    }
  }

  static Map<int, List<TAppointmentData>>
      _getAppointmentsPerMillis<TAppointmentData extends EasyAppointment>(
    final MessageWithMinAndMaxDate<List<TAppointmentData>> message,
  ) {
    final newAppointments = message.data;
    final minDate = message.minDate;
    final maxDate = message.maxDate;

    Map<int, List<TAppointmentData>> newAppointmentsPerMillis = {};

    for (final appointment in newAppointments) {
      newAppointmentsPerMillis = _addRecurrences(
        newAppointmentsPerMillis,
        appointment,
        minDate: minDate,
        maxDate: maxDate,
      );
    }

    return newAppointmentsPerMillis;
  }

  static Iterable<UtcDateTime>
      _newPreviousDates<TAppointmentData extends EasyAppointment>(
    final Map<int, ValueNotifier<List<TAppointmentData>>> appointmentsPerMillis,
  ) {
    final currentMillis = UtcDateTime.now().dayStart.millisecondsSinceEpoch;

    final newPrevMillis = appointmentsPerMillis.keys
        .where((final millis) => millis < currentMillis)
        .toList()
      ..sort();

    return newPrevMillis.map(UtcDateTime.fromMillisecondsSinceEpoch);
  }

  static Iterable<UtcDateTime>
      _newNextDates<TAppointmentData extends EasyAppointment>(
    final Map<int, ValueNotifier<List<TAppointmentData>>> appointmentsPerMillis,
  ) {
    final currentMillis = UtcDateTime.now().dayEnd.millisecondsSinceEpoch;

    final newNextMillis = appointmentsPerMillis.keys
        .where((final millis) => millis > currentMillis)
        .toList()
      ..sort();

    return newNextMillis.map(UtcDateTime.fromMillisecondsSinceEpoch);
  }

  void _addAppointment(final MapEntry<int, List<TAppointmentData>> entry) {
    _notifierFor(entry.key).value = entry.value;
  }

  void _removeAppointment(final MapEntry<int, List<TAppointmentData>> entry) {
    _removeNotifierFor(entry.key);
  }

  static Map<int, List<TAppointmentData>>
      _addRecurrences<TAppointmentData extends EasyAppointment>(
    final Map<int, List<TAppointmentData>> appointmentsPerDate,
    final TAppointmentData appointment, {
    required final UtcDateTime minDate,
    required final UtcDateTime maxDate,
  }) {
    final iterator = _buildIterator(
      appointment,
      minDate: minDate,
      maxDate: maxDate,
    );
    while (iterator.moveNext()) {
      // if (kDebugMode) {
      //   print('adding ${iterator.current}');
      // }
      final key = iterator.current.dayStart.millisecondsSinceEpoch;
      final _ = appointmentsPerDate.update(
        key,
        (final value) => [...value, appointment],
        ifAbsent: () => [appointment],
      );
    }

    return appointmentsPerDate;
  }

  int _keyFor(final UtcDateTime date) => date.dayStart.millisecondsSinceEpoch;

  static Iterator<UtcDateTime>
      _buildIterator<TAppointmentData extends EasyAppointment>(
    final TAppointmentData appointment, {
    required final UtcDateTime minDate,
    required final UtcDateTime maxDate,
  }) {
    if (appointment.startTime.isAfter(maxDate)) {
      return <UtcDateTime>[].iterator;
    }

    final rrule = appointment.recurrenceRule;

    if (rrule == null || rrule.isEmpty) {
      // Event is not recurring
      if (appointment.startTime.isBefore(minDate)) {
        // The single occurrence the event has is not in scope.
        return <UtcDateTime>[].iterator;
      }
      return <UtcDateTime>[appointment.startTime].iterator;
    }

    final appointmentStartDateTime = appointment.startTime.dateTime;

    final appointmentAfterDateTime = appointment.startTime.isBefore(minDate)
        ? minDate.dateTime
        : appointmentStartDateTime;

    return RecurrenceRule.fromString(
      (!rrule.startsWith('RRULE:')) ? 'RRULE:$rrule' : rrule,
    )
        .getInstances(
          start: appointmentStartDateTime,
          after: appointmentAfterDateTime,
          before: maxDate.dateTime,
          includeAfter: true,
        )
        .map(UtcDateTime.fromDateTime)
        .iterator;
  }
}
